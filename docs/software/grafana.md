# Grafana

[Grafana](https://grafana.com/) is a Time Series Database graphing application.

You can use it to visualize the Weather data imported by [influxdb_darksky](software/influxdb_darksky),
power, activity, and other data from [Home Assistant](software/homeassistant), and general server
information via Telegraf.

Grafana comes configured with a Dashboard for you out of the box.

You can login with the default user and pass you setup for HomelabOS.

## Access

It is available at [https://grafana.{{ domain }}/](https://grafana.{{ domain }}/) or [http://grafana.{{ domain }}/](http://grafana.{{ domain }}/)

It is also available via Tor at [http://grafana.{{ tor_domain }}/](http://grafana.{{ tor_domain }}/)